# Social Places PHP Symfony Test
Thank you, Social Places, for allowing me to take part of your test and providing an opportunity to learn a little bit more about the Symfony Framework.

I tried to provide something that although basic had just a little more flare than a simple form as per the brief.
- I added a touch of Bootstrap 4
- A little fetch from Facebook for user information (i.e. Email, First Name, Last Name)
- Data tables
- Used API platform for easy entity API resource

Additionally, I also tried to have a little bit of fun with the framework.

I feel as though there is much more that I could have done as the sky is the limit.