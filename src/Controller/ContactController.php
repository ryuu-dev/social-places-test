<?php

    namespace App\Controller;

    use App\Entity\Contact;
    use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
    use Omines\DataTablesBundle\Column\TextColumn;
    use Omines\DataTablesBundle\Controller\DataTablesTrait;
    use Omines\DataTablesBundle\DataTableFactory;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Component\HttpFoundation\Request;


    class ContactController extends AbstractController {

        private $dataTableFactory;

        public function __construct(DataTableFactory $dataTableFactory) {
            $this->dataTableFactory = $dataTableFactory;
        }

        /**
         * @Route("/contact", name="contact", methods={"GET"})
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
         */
        public function index(Request $request) {

            //DataTable of Contacts
            $table = $this->dataTableFactory->create()
                ->add('first_name', TextColumn::class, ['label' => 'First Name'])
                ->add('last_name', TextColumn::class, ['label' => 'Last Name'])
                ->add('email', TextColumn::class, ['label' => 'Email', 'render' => function($value, $context) { return "<a href='mailto:".$value."'>".$value."</a>"; }])
                ->add('contact_number', TextColumn::class, ['label' => 'Contact Number'])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => Contact::class,
                ])
                ->handleRequest($request);

            if ($table->isCallback()) {
                return $table->getResponse();
            }

            return $this->render('contact/index.html.twig', ['datatable' => $table]);
        }

        /**
         * @Route("/contact/create", name="contact", methods={"GET","HEAD"})
         */
        public function create() {
            $contact = new Contact();

            // Generating the form to display
            $form = $this->createFormBuilder($contact)
                ->add('first_name')
                ->add('last_name')
                ->add('contact_number')
                ->add('email')
                ->add('save', SubmitType::class, ['label' => 'Create Contact'])
                ->getForm();


            return $this->render('contact/create.html.twig', [
                'form' => $form->createView(),
                'sitekey' => getenv('GOOGLE_SITEKEY')
            ]);
        }


        /**
         * @Route("/contact/create", name="contact", methods={"POST"})
         * @param Request $request
         * @param \Swift_Mailer $mailer
         * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
         */
        public function store(Request $request, \Swift_Mailer $mailer) {
            $contact = new Contact();

            $form = $this->createFormBuilder($contact)
                ->add('first_name')
                ->add('last_name')
                ->add('contact_number')
                ->add('email')
                ->add('save', SubmitType::class, ['label' => 'Create Contact'])
                ->getForm();

            $form->handleRequest($request);

            // Checking for valid form and if the Google recapture v2 is successful
            if ($this->validateCaptcha($request->get('g-recaptcha-response'))) {
                if ($form->isSubmitted() && $form->isValid()) {
                    $formData = $form->getData();


                    // Saving form data to mySQL database
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($formData);
                    $entityManager->flush();

                    // Contact message to send
                    $contactMessage = (new \Swift_Message("Hello ".$formData->getFirstName(), "Some sort of introductory text"))
                        ->addFrom("no-reply@ryuu.co.za")
                        ->addTo($formData->getEmail(), $formData->getFirstName().' '.$formData->getLastName());

                    // Admin message to send
                    $adminMessage = (new \Swift_Message($formData->getFirstName()." has been added"))
                        ->addFrom("no-reply@ryuu.co.za")
                        ->addTo(getenv("ADMIN_EMAIL"), getenv("ADMIN_NAME"))
                        ->setBody(
                            $this->render('emails/admin-email.html.twig', [
                                'contact' => $formData
                            ]),
                            'text/html'
                        );

                    // Normally this should be added to a queue (like SQS) for optimization and speed efficiency
                    // Send emails to admin and added contact
                    $mailer->send($contactMessage);
                    $mailer->send($adminMessage);

                    // Display success view
                    return $this->render('contact/success.html.twig', [
                        'contact' => $formData
                    ]);
                } else {
                    $this->addFlash('danger', 'There was an issue with your form, please try again');
                    return $this->render('contact/create.html.twig', [
                        'form' => $form->createView(),
                        'sitekey' => getenv('GOOGLE_SITEKEY')
                    ]);
                }
            } else {
                $this->addFlash('danger', 'Ensure the ReCapture has been selected');
            }

            // Redirect back to the form
            return $this->redirect('/contact/create');
        }

        /**
         * Google ReCapture v2 check
         * @param $token : string|null
         * @return bool
         */
        private function validateCaptcha($token): bool {
            if (!isset($token)) {
                return false;
            }

            $url = "https://www.google.com/recaptcha/api/siteverify";
            // Using cURL (alternatively GuzzleHttp could be used)
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                "secret" => getenv("GOOGLE_SECRET"), "response" => $token));
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);

            return $data->success;
        }
    }
