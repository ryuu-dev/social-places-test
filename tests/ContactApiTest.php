<?php

    namespace App\Tests;

    use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

    class ContactApiTest extends WebTestCase {
        public function testIndex() {
            $client = static::createClient();
            $crawler = $client->request('GET', '/api/contacts', [], [], ['CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json']);

            $this->assertSame(200, $client->getResponse()->getStatusCode());

            $this->assertJson($client->getResponse()->getContent());
        }


        public function testCreate() {
            $json_data = json_encode([
                                         'firstName' => 'Test First',
                                         'lastName' => 'Test Last',
                                         'contactNumber' => '0123456789',
                                         'email' => 'james@ryuu.co.za'
                                     ]);

            $client = static::createClient();
            $client->request('POST', '/api/contacts', [], [], ['CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json'], $json_data);

            $this->assertSame(201, $client->getResponse()->getStatusCode());
            $this->assertJson($client->getResponse()->getContent());

            dd($client->getResponse()->getContent());
        }


        public function testCreateFailures() {
            // Testing bad phone number
            $json_data = json_encode([
                                         'firstName' => 'Test First',
                                         'lastName' => 'Test Last',
                                         'contactNumber' => 'bad phone number',
                                         'email' => 'james@ryuu.co.za'
                                     ]);

            $client = static::createClient();
            $client->request('POST', '/api/contacts', [], [], ['CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json'], $json_data);

            $this->assertSame(400, $client->getResponse()->getStatusCode());
            $this->assertJson($client->getResponse()->getContent());
            $violations = json_decode($client->getResponse()->getContent())->violations;
            $this->assertEquals(1, count($violations));
            $this->assertEquals('contact_number', $violations[0]->propertyPath);

            // Testing missing data
            $json_data = [
                'firstName' => 'Test First',
                'lastName' => 'Test Last',
                'contactNumber' => '0123456789',
                'email' => 'james@ryuu.co.za'
            ];

            foreach (['first_name' => 'firstName', 'last_name' => 'lastName', 'email' => 'email'] as $key => $value) {
                $missing_data = $json_data;
                $missing_data[$value] = '';

                $client = static::createClient();
                $client->request('POST', '/api/contacts', [], [], ['CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json'], json_encode($missing_data));
                $this->assertSame(400, $client->getResponse()->getStatusCode());
                $this->assertJson($client->getResponse()->getContent());
                $violations = json_decode($client->getResponse()->getContent())->violations;
                $this->assertEquals(1, count($violations));
                $this->assertEquals($key, $violations[0]->propertyPath);
            }
        }
    }
