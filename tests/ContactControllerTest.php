<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/contact');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Contacts', $crawler->filter('h1')->text());
    }

    public function testCreate()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/contact/create');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Create Contact', $crawler->filter('h1')->text());

        $form = $crawler->filter('#form_save')->form();
        $form['form[first_name]'] = "Ryuu";
        $form['form[last_name]'] = "Skylark";
        $form['form[email]'] = "james@ryuu.co.za";

        $crawler = $client->submit($form);
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        $crawler = $client->followRedirect();

        $this->assertSame(200, $client->getResponse()->getStatusCode());

        $this->assertContains('Create Contact', $crawler->filter('h1')->text());
        $this->assertContains('Ensure the ReCapture has been selected', $crawler->filter('.alert.alert-danger')->text());

    }
}
